const funciones = require ('../librerias/mis_funciones');
const archivos_list = require('../librerias/list_file');
const archivos_generar = require('../librerias/crear_archivo');
const leer_fs = require('../librerias/leer_archivos');

let suma = funciones.sumar(1,4);
console.log('El resultado de la suma es:', suma);

console.log(isNaN(suma));

if(isNaN(suma) === false){
    console.log("El resultado de la suma es:" + suma,funciones.msg_ok);
}else{
    console.log(suma,funciones.msg_fail);

}

let resta = funciones.restar(8,4);
console.log('El resultado de la resta es:', resta);

console.log(isNaN(resta));

if(isNaN(resta) === false){
    console.log("El resultado de la resta es:" + resta,funciones.msg_ok);
}else{
    console.log(resta,funciones.msg_fail);
}

let multiplica = funciones.multiplicar(4,4);
console.log('El resultado de la multiplicacion es:', multiplica);

console.log(isNaN(multiplica));

if(isNaN(multiplica) === false){
    console.log("El resultado de la multiplicacion es:" + multiplica,funciones.msg_ok);
}else{
    console.log(multiplica,funciones.msg_fail);
}


let divida = funciones.dividir(10,5);
console.log('El resultado de la division es:', divida);

console.log(isNaN(divida));

if(isNaN(divida) === false){
    console.log("El resultado de la division es:" + divida,funciones.msg_ok);
}else{
    console.log(divida,funciones.msg_fail);
}


archivos_list.mostrar_archivos();

//GENERAR ARCHIVO A PARTIR DE LOS HOBBIES

const hobbies = ['Futbol','Escuchar Musica','Tocar la guitarra','Tennis','Natacion'];
hobbies.forEach(item =>{
archivos_generar.crear_archivo_plano("log.txt",item);

});

//LEER ARCHIVO

var path_file = process.cwd() + "//" + "log.txt";

console.log('path archivo',path_file);
let data = leer_fs.leer_archivo(path_file);
console.log(data);