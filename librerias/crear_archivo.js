const fs = require('fs');

//funcion que permite creacion de archivos
//Params: file > el nombre del archivo, por ejemplo log.txt
//Params: text > el contenido de archivo, hola, saludos,...etc

function crear_archivo_plano(file,text){
    fs.appendFile(file,text + "\n", function(err){
    if(err){
        return console.log(err)
    } 
    });
}

exports.crear_archivo_plano = crear_archivo_plano;